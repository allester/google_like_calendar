const VISIBLE_EVENT_THRESHOLD = 2
const USE_THRESHOLD = true

function AttachToWeek(objEvent, $_a) {
    // each row is a week, so determine which row/week to attach to
    let $_row = document.querySelector(".mCalendar-row.offset-"+objEvent.wk)

    if (USE_THRESHOLD) {
        
        // attached if adding another event would not exceed the threshold

        if (arrEventCounter[objEvent.from] + 1 <= VISIBLE_EVENT_THRESHOLD) {
            // append the link to the calendar
            $_row.appendChild($_a)

            // increment the threshold counter everyday the event lasts

            let intSpan = $_a.getAttribute("data-span")

            intSpan = parseInt(intSpan)

            if (intSpan == 0) {
                intSpan += 1
            }

            for (let x = objEvent.from; x < (objEvent.from + intSpan); x++) {
                IncrementEventCounter(x, 1)
            }

        } else {
            
            // when adding an event would exceed the allowed threshold,
			// instead of adding it to the week as an event,
			// find or create a message in each day (i.e. calendar cell) the event would overflow
			// that displays the count of overflow events

			// calculate the number of overflow messages to append

            let intOffset = objEvent.CalculateOffset()

            if (intOffset < 7) {
                intOverflowNotesToAppend = 7 - intOffset
            } else {
                intOverflowNotesToAppend = 1
            }

            // loop over the number of times it is necessary to append this to the calendar row
			// for the given week; subtract one from the loop control variable because the loop
			// starts at zero; the loop must start at zero, or all overflow notes would be one
			// day off, i.e. they would be inserted a day after they should have been

            for (let x = 0; x <= intOverflowNotesToAppend - 1; x++) {
                if (objEvent.from + x <= objEvent.to) {
                    let intCount = 0

                    // add x to the offset so that the overflow note gets appended to the correct day of the week
					// the variable will be null if querySelector cannot find such an element
                    let $_overflowElement = $_row.querySelector(".overflowElement.offset-" + (intOffset + x))

                    // if the overflow note element could not be found, create one with the adjusted offset
                    if ($_overflowElement == null) {
                        intCount = 1
                        $_overflowElement = CreateOverflowElement(objEvent, intOffset + x)
                        $_row.appendChild($_overflowElement)
                    } else {
                        intCount = parseInt($_overflowElement.getAttribute("data-count"))
                        intCount += 1
                        $_overflowElement.setAttribute("data-count", intCount)
                    }

                    if (intCount > 1) {
                        $_overflowElement.innerText = $_overflowElement.getAttribute("data-count") + " more items"
                    }

                    // increment the threshold counter because the overflow note consumes a slot, too
                    IncrementEventCounter(objEvent.from + x, 1)

                }
            }

        }

    } else {
        $_row.appendChild($_a)
    }
}

function CreateOverflowElement(objEvent, offset) {
    let $_oe = document.createElement("DIV")
    $_oe.classList.add("overflowElement")
    $_oe.classList.add("offset-" + offset)
    $_oe.setAttribute("data-count", 1)
    $_oe.innerText = "1 more item"
    return $_oe
}

function CreateEventSpan(objEvent) {
    let $_a = document.createElement("A")

    $_a.href = "#" // change to objEvent.href when it goes live

    $_a.innerText = objEvent.desc

    $_a.classList.add("colorscheme-" + objEvent.calendarID)

    $_a.classList.add("event")

    //-----------------------------------------------------------------------
	// if the event occurs in week 1, but the month doesn't start on Sunday,
	// shift any event that happens before the 1st of the month by a number
	// of days equal to the offset
	//-----------------------------------------------------------------------

    let offset = objEvent.CalculateOffset()

    $_a.classList.add("offset-" + offset)

    let multiday = Max7(objEvent.span)

    if (objEvent.span > 1) {
        $_a.classList.add("multiday-" + multiday)
    }

    let intSpan = multiday - offset

    if (intSpan < 1) {
        intSpan = 0
    }

    $_a.setAttribute("data-span", intSpan)

    $_a.setAttribute("data-eventid", objEvent.eventID)

    return $_a

}

function Max7(i) {
    if (i > 7) {
        return 7
    }
    return i
}

function CalendarEvent(wk,dw,span,wrap,desc,calendarID,eventID,offsetEmptyStartOfMonth,from,to) {
    this.isClone = false
    this.from = from
    this.to = to
    this.offsetEmptyStartOfMonth = offsetEmptyStartOfMonth
    this.wk = wk
    this.dw = dw
    this.span = span
    this.desc = desc
    this.calendarID = calendarID
    this.eventID = eventID
    this.wrap = wrap
    this.CalculateWrap = function (dw, span) {
        let make7 = 7 - (dw - 1)
        let remainderOfSpan = span - make7
        return remainderOfSpan
    }
    this.href = calendarID + ".example.com/events/" + eventID // must change to the deployment domain once it goes live

    this.CalculateOffset = function () {
        if (this.wk == 1 && this.dw <= this.offsetEmptyStartOfMonth) {
            return this.dw + this.offsetEmptyStartOfMonth - 1
        } else {
            return this.dw - 1
        }
    }
    return this
}

function CreateEmptyEventCounterArray () {
    return [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]; // 31 total slots
}

function IncrementEventCounter (day, inc) {
    arrEventCounter[day] += inc
}

//-----------------------------------------------------------------------
// each item in the array is an event that will be plotted on the calendar
//-----------------------------------------------------------------------

let arrEvents = [
    new CalendarEvent(1, 1, 1, null, "Alpha", 'event1', 21, 4, 1, 1),
    new CalendarEvent(1, 2, 1, null, "Alpha", 'event1', 21, 4, 2, 2),
    new CalendarEvent(1, 6, 15, null, "Bravo", 'event2', 26, 4, 2, 16),
    new CalendarEvent(3, 6, 8, null, "Charlie", 'event3', 21, 4, 16, 23),
    new CalendarEvent(3, 6, 8, null, "Delta", 'event1', 21, 4, 16, 23),
    new CalendarEvent(3, 6, 8, null, "Echo", 'event4', 21, 4, 16, 23),
    new CalendarEvent(3, 6, 8, null, "Foxtrot", 'event5', 21, 4, 16, 23),
    new CalendarEvent(3, 7, 7, null, "Golf", 'event6', 21, 4, 17, 23),
    new CalendarEvent(2, 2, 3, null, "Hotel", 'event7', 21, 4, 5, 7)
];

//-----------------------------------------------------------------------
// This array keeps track of how many events are in a day, e.g.
// 	[0] will always be zero
// 	[29] will always be zero during February on non-leap years
// 	[31] will always be zero during months without a 31st day
// The number inside each is incremented every time an event that occurs on the date
//  is added to the calendar.
//-----------------------------------------------------------------------

let arrEventCounter = CreateEmptyEventCounterArray()

//-----------------------------------------------------------------------
// Loop through each event; print them to the calendar.  Events that wrap
// weeks are split into clones
//-----------------------------------------------------------------------

for (let x = 0; x < arrEvents.length; x++) {

    let objEvent = arrEvents[x]

    let $_a = CreateEventSpan(objEvent)

    objEvent.wrap = objEvent.CalculateWrap(objEvent.dw, objEvent.span)

    if (objEvent.wrap > 0) {
        $_a.classList.add("weekWraps")
    }

    AttachToWeek(objEvent, $_a)

    if (objEvent.wrap > 0) {
        let numWeeksToWrap = Math.floor(objEvent.wrap / 7)

        let eventClone = new CalendarEvent(objEvent.wk, objEvent.dw, objEvent.span, objEvent.wrap, objEvent.desc, objEvent.calendarID, objEvent.eventID, objEvent.offsetEmptyStartOfMonth, objEvent.from, objEvent.to)

        do {
            eventClone = new CalendarEvent(eventClone.wk, eventClone.dw, eventClone.span, eventClone.wrap, eventClone.desc, eventClone.calendarID, eventClone.eventID, eventClone.offsetEmptyStartOfMonth, eventClone.from, eventClone.to)

            // set as a clone so the script knows ...
            eventClone.isClone = true

            // pushes to the next week
            eventClone.wk += 1

            // starts over on Sunday
            eventClone.dw = 1

            // alter the 'from' and 'to' properties
            eventClone.from = eventClone.span - eventClone.wrap + eventClone.from

            // this must come before the "from" assignment
            eventClone.to = eventClone.from + Max7(eventClone.wrap) - 1

            // how many days will the event wrap into the next week?
            if (eventClone.wrap >= 7) {
                eventClone.span = eventClone.span - (eventClone.span - eventClone.wrap)
                eventClone.wrap = eventClone.wrap - 7
            } else {
                eventClone.span = eventClone.wrap
                eventClone.wrap = 0
            }

            let $_cloneA = CreateEventSpan(eventClone)

            // flatten the rounded corners on the left side
            $_cloneA.classList.add("weekWrapped")

            // flatten the rounded corners on the right side
            if (eventClone.wrap > 0) {
                $_cloneA.classList.add("weekWraps")
            }

            // appends the event to the DOM
            AttachToWeek(eventClone, $_cloneA)

            numWeeksToWrap -= 1

        } while (numWeeksToWrap > -1);

    }

}